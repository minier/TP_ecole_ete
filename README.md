## TP Ecole Ete

# File sujet_TP.pdf
The subject of the TP. You could start be here.

# Repository Choco2

This repository contains all the source files regarding the Choco experiments.

# Repository SK-Step1-enum

This repository contains all the Step1 solutions according the number of r, the number of rounds and nb, the number of active Sboxes.

# File SK-Step1_missing

This is the Step 1 in MiniZinc.

To launch experiments for Step 1. You can:
-) Load the MiniZinc IDE
-) Or compile using a command line:
```bash
minizinc -c --solver Gecode -D "r=5;" SK-Step1_missing.mzn
```
where r is the number of rounds you want.

# LAUNCHING THE EXPERIMENTS

The repository Choco2 contains the main java files required to launch the Step 2. 
There are 2 repository 1, Step2, for the SKINNY-128 version, 1, Step264, for SKINNY-64.
You need to compile those files with choco-solver-4.10.6-jar-with-dependencies.jar.

More precisely, to compile, go to the good directory and type:
```bash
javac -cp choco-solver-4.10.6-jar-with-dependencies.jar *.java
```
to run:
```bash
java -cp .:choco-solver-4.10.6-jar-with-dependencies.jar SKStep2 *Nom du fichier source solutions Step1* Nbr 0 1
```
where Nbr is the number of rounds.

The program will produce a file in the repository results indicating the minimal probability.
