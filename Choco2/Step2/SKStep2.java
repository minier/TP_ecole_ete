
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;


//Step2 Single Key to find a best instantiation
//launch SKStep2Inst in Parallel (relying on SolvingRace) to search for the best solutions among all the solution listed in the inputFile


public class SKStep2 {

    public static void main(String[] args) {
        long start = System.currentTimeMillis();
        String inputFile = args[0];
        int NBROUNDS = Integer.parseInt(args[1]);
        int OBJBOUND = Integer.parseInt(args[2]); // to use the default bound (70*NBSB) set OBJBOUND = 0
        int NBTHREADS = 8;
        if (args.length == 4) {
            NBTHREADS = Integer.parseInt(args[3]);
        }
        System.out.println("SKStep2 r=" + args[1] + " with bound = " + args[2]);
        BufferedReader in = null;
        try {
            in = new BufferedReader(new FileReader(inputFile));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        parse(in, NBROUNDS, OBJBOUND, NBTHREADS);
        long end = System.currentTimeMillis();
        System.out.println("time for SKStep2:" + NBROUNDS + " " + (end - start) + " in milliseconds");


    }

    public static void parse(BufferedReader inF, int NBROUNDS, int OBJBOUND, int NBTHREADS) {
        int cpt = 0, cpt2 = 0;
        int NBSB = 0;
        String line = null, line2 = null;
        int[][][] DX = new int[NBROUNDS][4][4];

        String outputFile = "results/step2/SK/SK-Step2-r" + NBROUNDS + "nb" + NBSB;
        StringBuffer buffer = new StringBuffer();
        SolvingRace track = new SolvingRace(NBTHREADS, buffer, outputFile);

        try {
            while ((line = inF.readLine()) != null) {
                if (line.startsWith("DX")) {
                    cpt2 = 0;
                    line2 = line.substring(line.indexOf("=") + 1, line.indexOf(",End"));
                    String[] tab = line2.split(",");
                    cpt++;
                    for (int k = 0; k < NBROUNDS; k++) {
                        for (int i = 0; i < 4; i++) {
                            for (int j = 0; j < 4; j++) {
                                DX[k][i][j] = Integer.parseInt(tab[cpt2++].trim());
                                if (DX[k][i][j] == 1) {
                                    NBSB++;
                                }
                            }
                        }
                    }
                }
                if (cpt == 1) {
                    if (OBJBOUND == 0) {
                        OBJBOUND = 70 * NBSB;
                    }
                    int finalNBSB = NBSB;
                    int finalOBJBOUND = OBJBOUND;
                    int[][][] finalDX = Tools.deepCopy(DX);
                    track.addModel(
                            () -> SKStep2Factory.enumerate(buffer, finalDX, NBROUNDS, finalNBSB, finalOBJBOUND));
                    NBSB = 0;
                    cpt = 0;
                }
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();

        } catch (IOException e) {
            e.printStackTrace();
        }
        track.run();

    }

}
		

