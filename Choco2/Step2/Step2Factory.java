/**
 * Copyright (c) 1999-2020, Ecole des Mines de Nantes
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * <p>
 * * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * * Neither the name of the Ecole des Mines de Nantes nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 * <p>
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

import org.chocosolver.solver.Model;
import org.chocosolver.solver.constraints.Constraint;
import org.chocosolver.solver.constraints.extension.Tuples;
import org.chocosolver.solver.variables.IntVar;

/**
 * <br/>
 *
 * @author Charles Prud'homme
 * @since 01/04/2020
 */
public class Step2Factory {

    protected static final String EOL = System.getProperty("line.separator");

    protected static final int MAXVAL = 255;

    private static final int[] Sbox8 = new int[]{
            0x65, 0x4c, 0x6a, 0x42, 0x4b, 0x63, 0x43, 0x6b, 0x55, 0x75, 0x5a, 0x7a, 0x53, 0x73, 0x5b, 0x7b,
            0x35, 0x8c, 0x3a, 0x81, 0x89, 0x33, 0x80, 0x3b, 0x95, 0x25, 0x98, 0x2a, 0x90, 0x23, 0x99, 0x2b,
            0xe5, 0xcc, 0xe8, 0xc1, 0xc9, 0xe0, 0xc0, 0xe9, 0xd5, 0xf5, 0xd8, 0xf8, 0xd0, 0xf0, 0xd9, 0xf9,
            0xa5, 0x1c, 0xa8, 0x12, 0x1b, 0xa0, 0x13, 0xa9, 0x05, 0xb5, 0x0a, 0xb8, 0x03, 0xb0, 0x0b, 0xb9,
            0x32, 0x88, 0x3c, 0x85, 0x8d, 0x34, 0x84, 0x3d, 0x91, 0x22, 0x9c, 0x2c, 0x94, 0x24, 0x9d, 0x2d,
            0x62, 0x4a, 0x6c, 0x45, 0x4d, 0x64, 0x44, 0x6d, 0x52, 0x72, 0x5c, 0x7c, 0x54, 0x74, 0x5d, 0x7d,
            0xa1, 0x1a, 0xac, 0x15, 0x1d, 0xa4, 0x14, 0xad, 0x02, 0xb1, 0x0c, 0xbc, 0x04, 0xb4, 0x0d, 0xbd,
            0xe1, 0xc8, 0xec, 0xc5, 0xcd, 0xe4, 0xc4, 0xed, 0xd1, 0xf1, 0xdc, 0xfc, 0xd4, 0xf4, 0xdd, 0xfd,
            0x36, 0x8e, 0x38, 0x82, 0x8b, 0x30, 0x83, 0x39, 0x96, 0x26, 0x9a, 0x28, 0x93, 0x20, 0x9b, 0x29,
            0x66, 0x4e, 0x68, 0x41, 0x49, 0x60, 0x40, 0x69, 0x56, 0x76, 0x58, 0x78, 0x50, 0x70, 0x59, 0x79,
            0xa6, 0x1e, 0xaa, 0x11, 0x19, 0xa3, 0x10, 0xab, 0x06, 0xb6, 0x08, 0xba, 0x00, 0xb3, 0x09, 0xbb,
            0xe6, 0xce, 0xea, 0xc2, 0xcb, 0xe3, 0xc3, 0xeb, 0xd6, 0xf6, 0xda, 0xfa, 0xd3, 0xf3, 0xdb, 0xfb,
            0x31, 0x8a, 0x3e, 0x86, 0x8f, 0x37, 0x87, 0x3f, 0x92, 0x21, 0x9e, 0x2e, 0x97, 0x27, 0x9f, 0x2f,
            0x61, 0x48, 0x6e, 0x46, 0x4f, 0x67, 0x47, 0x6f, 0x51, 0x71, 0x5e, 0x7e, 0x57, 0x77, 0x5f, 0x7f,
            0xa2, 0x18, 0xae, 0x16, 0x1f, 0xa7, 0x17, 0xaf, 0x01, 0xb2, 0x0e, 0xbe, 0x07, 0xb7, 0x0f, 0xbf,
            0xe2, 0xca, 0xee, 0xc6, 0xcf, 0xe7, 0xc7, 0xef, 0xd2, 0xf2, 0xde, 0xfe, 0xd7, 0xf7, 0xdf, 0xff,
    };

    public static boolean EXTENSION = false;

    private static final Tuples tupleXor = createRelationXor();

    protected static final Tuples tupleSB = createRelationSbox();

    private static Tuples createRelationXor() {
        Tuples tuplesXor = new Tuples(true);
        for (int i = 0; i < MAXVAL + 1; i++) {
            for (int j = 0; j < MAXVAL + 1; j++) {
                tuplesXor.add(i, j, i ^ j);
            }
        }
        return tuplesXor;
    }

    protected static Tuples createRelationSbox() {
        int[][] ddt = new int[MAXVAL + 1][MAXVAL + 1];
        for (int i = 0; i < MAXVAL + 1; i++)
            for (int j = 0; j < MAXVAL + 1; j++) ddt[i][j] = 0;

        for (int i = 0; i < MAXVAL + 1; i++) {
            for (int j = 0; j < MAXVAL + 1; j++) {
                {
                    ddt[i][Sbox8[j] ^ Sbox8[i ^ j]]++;
                }
            }
        }


        Tuples tuplesSB = new Tuples(true);
        tuplesSB.add(0, 0, 0);


        for (int i = 1; i < MAXVAL + 1; i++) {
            for (int j = 1; j < MAXVAL + 1; j++) {
                if (ddt[i][j] != 0) {
                    if (ddt[i][j] == 2) {
                        tuplesSB.add(i, j, 70);
                    } //10 --> 70
                    if (ddt[i][j] == 4) {
                        tuplesSB.add(i, j, 60);
                    }  // 20 --> 60
                    if (ddt[i][j] == 6) {
                        tuplesSB.add(i, j, 54);
                    }  // 26 --> 54

                    if (ddt[i][j] == 8) {
                        tuplesSB.add(i, j, 50);
                    }  // 30 --> 50
                    if (ddt[i][j] == 12) {
                        tuplesSB.add(i, j, 44);
                    }  // 36 --> 44
                    if (ddt[i][j] == 16) {
                        tuplesSB.add(i, j, 40);
                    }  // 40 --> 40
                    if (ddt[i][j] == 20) {
                        tuplesSB.add(i, j, 37);
                    }  // 43 --> 37

                    if (ddt[i][j] == 24) {
                        tuplesSB.add(i, j, 34);
                    } // 46 --> 34
                    if (ddt[i][j] == 28) {
                        tuplesSB.add(i, j, 32);
                    } // 48 --> 32

                    if (ddt[i][j] == 32) {
                        tuplesSB.add(i, j, 30);
                    } // 50 --> 30
                    if (ddt[i][j] == 40) {
                        tuplesSB.add(i, j, 27);
                    } // 53 --> 27
                    if (ddt[i][j] == 48) {
                        tuplesSB.add(i, j, 24);
                    } // 56 --> 24

                    if (ddt[i][j] == 64) {
                        tuplesSB.add(i, j, 20);
                    } // 60 --> 20
                }
            }
        }


        return tuplesSB;
    }


    protected static void postXOR(Model model, IntVar a, IntVar b, IntVar c) {
        if (EXTENSION) {
            model.table(new IntVar[]{a, b, c}, tupleXor).post();
        } else {
            new Constraint("XOR", new PropXOR(a, b, c)).post();
        }
    }

    // Add the xor constraint by considering the zeros. If there is a zero, the two other variables are equal
    protected static void postXOR(Model m, IntVar a, boolean a_zero, IntVar b, boolean b_zero, IntVar c, boolean c_zero) {
        if (a_zero) {
            b.eq(c).post();
        } else if (b_zero) {
            a.eq(c).post();
        } else if (c_zero) {
            a.eq(b).post();
        } else {
            postXOR(m, a, b, c);
        }
    }


    protected static void postMC(Model m, IntVar a, boolean a_zero, IntVar b, boolean b_zero, IntVar c, boolean c_zero, IntVar d, boolean d_zero, IntVar e, boolean e_zero, IntVar f,
                                 boolean f_zero, IntVar g, boolean g_zero, IntVar h, boolean h_zero) {
        a.eq(f).post();
        postXOR(m, b, b_zero, c, c_zero, g, g_zero);
        postXOR(m, a, a_zero, c, c_zero, h, h_zero);
        postXOR(m, d, d_zero, e, e_zero, h, h_zero);
    }


}
