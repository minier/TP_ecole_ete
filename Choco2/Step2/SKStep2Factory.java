/**
 * Copyright (c) 1999-2020, Ecole des Mines de Nantes
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * <p>
 * * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * * Neither the name of the Ecole des Mines de Nantes nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 * <p>
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

import org.chocosolver.cutoffseq.LubyCutoffStrategy;
import org.chocosolver.solver.Model;
import org.chocosolver.solver.Solver;
import org.chocosolver.solver.search.loop.monitors.IMonitorSolution;
import org.chocosolver.solver.search.strategy.Search;
import org.chocosolver.solver.search.strategy.selectors.values.IntDomainMin;
import org.chocosolver.solver.search.strategy.selectors.variables.DomOverWDeg;
import org.chocosolver.solver.variables.IntVar;
import org.chocosolver.util.tools.ArrayUtils;

/**
 * <br/>
 *
 * @author Charles Prud'homme
 * @since 01/04/2020
 */
public class SKStep2Factory extends Step2Factory {

    static {
        EXTENSION = true;
    }

    public static Model enumerate(StringBuffer buffer, int[][][] DX, int NBROUNDS, int NBSB, int OBJBOUND) {
        //Model
        Model model = new Model();
        IntVar[][][] dX = new IntVar[NBROUNDS][4][4];
        IntVar[][][] dSB = new IntVar[NBROUNDS][4][4];
        IntVar[][][] prob = new IntVar[NBROUNDS][4][4];


        // Initialization

        for (int r = 0; r < NBROUNDS; r++) {
            for (int i = 0; i < 4; i++) {
                for (int j = 0; j < 4; j++) {
                    if (DX[r][i][j] == 0) {
                        dX[r][i][j] = model.intVar(0);
                        dSB[r][i][j] = model.intVar(0);
                    } else {
                        dX[r][i][j] = model.intVar(1, MAXVAL);
                        dSB[r][i][j] = model.intVar(1, MAXVAL);
                    }
                }
            }
        }


        for (int r = 0; r < NBROUNDS; r++) {
            for (int i = 0; i < 4; i++) {
                for (int j = 0; j < 4; j++) {
                    if (DX[r][i][j] == 0) {
                        prob[r][i][j] = model.intVar(0);
                    } else {
                        prob[r][i][j] = model.intVar(20, 70);
                    }
                }
            }
        }

        // Objective

        IntVar obj = model.intVar(20 * NBSB, Math.min(70 * NBSB, OBJBOUND));
        model.sum(ArrayUtils.flatten(prob), "=", obj).post();


        // constraints shiftrows - 0 1 2 3 7 4 5 6 10 11 8 9 13 14 15 12
        // + constraints mix columns
        for (int r = 0; r < NBROUNDS - 1; r++) {
            for (int j = 0; j < 4; j++) {
                postMC(model,
                        dSB[r][0][j], DX[r][0][j] == 0,
                        dSB[r][1][(3 + j) % 4], DX[r][1][(3 + j) % 4] == 0,
                        dSB[r][2][(2 + j) % 4], DX[r][2][(2 + j) % 4] == 0,
                        dSB[r][3][(1 + j) % 4], DX[r][3][(1 + j) % 4] == 0,
                        dX[r + 1][0][j], DX[r + 1][0][j] == 0,
                        dX[r + 1][1][j], DX[r + 1][1][j] == 0,
                        dX[r + 1][2][j], DX[r + 1][2][j] == 0,
                        dX[r + 1][3][j], DX[r + 1][3][j] == 0);
            }
        }


        // Declare constraint: SB

        for (int r = 0; r < NBROUNDS; r++) {
            for (int i = 0; i < 4; i++) {
                for (int j = 0; j < 4; j++) {
                    if (DX[r][i][j] != 0) {
                        model.table(new IntVar[]{dX[r][i][j], dSB[r][i][j], prob[r][i][j]}, tupleSB).post();
                    }
                }
            }
        }


        //2. Solving part

        IntVar[] vars = new IntVar[NBROUNDS * 4 * 4 * 2];
        int cvars = 0;


        for (int r = 0; r < NBROUNDS; r++) {
            for (int i = 0; i < 4; i++) {
                for (int j = 0; j < 4; j++) {
                    vars[cvars++] = dSB[r][i][j];
                    vars[cvars++] = dX[r][i][j];
                }
            }
        }


        model.setObjective(Model.MINIMIZE, obj);
        Solver solver = model.getSolver();
        solver.printShortFeatures();

        solver.setSearch(
                Search.lastConflict(
                        new DomOverWDeg(vars, 0, new IntDomainMin())));


        solver.setRestarts(l -> solver.getFailCount() >= l, new LubyCutoffStrategy(2), 256);
        solver.setNoGoodRecordingFromRestarts();
        solver.showShortStatistics();

        model.addHook("PRINTER", (IMonitorSolution) () -> {
            buffer.append("OBJ =").append(obj.getValue()).append(EOL);

            for (int r = 0; r < dX.length; r++) {
                buffer.append("Round ").append(r).append(EOL);
                for (int i = 0; i < 4; i++) {
                    buffer.append(dX[r][i][0].getValue()).append(" ").append(dX[r][i][1].getValue()).append(" ").append(dX[r][i][2].getValue()).append(" ").append(dX[r][i][3].getValue());
                    buffer.append("    ").append(dSB[r][i][0].getValue()).append(" ").append(dSB[r][i][1].getValue()).append(" ").append(dSB[r][i][2].getValue()).append(" ").append(dSB[r][i][3].getValue());
                    buffer.append(EOL);
                }
            }

            buffer.append("==========").append(EOL);
        });
        return model;
    }


}
