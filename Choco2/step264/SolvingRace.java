
/*
 * This file is part of choco-solver, http://choco-solver.org/
 *
 * Copyright (c) 2019, IMT Atlantique. All rights reserved.
 *
 * Licensed under the BSD 4-clause license.
 *
 * See LICENSE file in the project root for full license information.
 */

import org.chocosolver.solver.Model;
import org.chocosolver.solver.ResolutionPolicy;
import org.chocosolver.solver.Solver;
import org.chocosolver.solver.exception.SolverException;
import org.chocosolver.solver.search.loop.monitors.IMonitorSolution;
import org.chocosolver.solver.variables.IntVar;
import org.chocosolver.solver.variables.Variable;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Supplier;

/**
 * <p>
 * A Portfolio helper.
 * Project: choco.
 *
 * @author Charles Prud'homme
 * @since 11/12/2019.
 */
public class SolvingRace {

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////       VARIABLES       //////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    AtomicInteger bestBound = new AtomicInteger(9999999);
    /**
     * List of {@link Model}s to be executed in parallel.
     */
    private final List<Model> models;

    private final List<Supplier<Model>> suppliers;

    /**
     * Nb max threads to use.
     */
    private int nThreads;

    private final StringBuffer buffer;
    private final String outputFile;

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////      CONSTRUCTOR      //////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Creates a new ParallelPortfolio
     * This class stores the models to be executed in parallel in a {@link ArrayList} initially empty.
     */
    public SolvingRace(StringBuffer buffer, String outputFile) {
        this(9999, buffer, outputFile);
    }

    /**
     * Creates a new ParallelPortfolio
     * This class stores the models to be executed in parallel in a {@link ArrayList} initially empty.
     *
     * @param nThreads the number of threads in the pool (upper bound)
     */
    public SolvingRace(int nThreads, StringBuffer buffer, String outputFile) {
        this.models = new ArrayList<>();
        this.suppliers = new ArrayList<>();
        this.nThreads = nThreads;
        this.buffer = buffer;
        this.outputFile = outputFile;
        Path parentDir = Paths.get(outputFile).getParent();
        if (!Files.exists(parentDir)) {
            try {
                Files.createDirectories(parentDir);
                Files.deleteIfExists(Paths.get(outputFile));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////          API          //////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * <p>
     * Adds a model to the list of models to run in parallel.
     * The model can either be a fresh one, ready for populating, or a populated one.
     * </p>
     * <p>
     * <b>Important:</b>
     *  <ul>
     *      <li>the populating process is not managed by this ParallelPortfolio
     *  and should be done externally, with a dedicated method for example.
     *  </li>
     *  <li>
     *      when dealing with optimization problems, the objective variables <b>HAVE</b> to be declared eagerly with
     *      {@link Model#setObjective(boolean, Variable)}.
     *  </li>
     *  </ul>
     *
     * </p>
     *
     * @param supplier a model to add
     */
    public void addModel(Supplier<Model> supplier) {
        this.suppliers.add(supplier);
    }

    /**
     * Run the solve() instruction of every model of the portfolio in parallel.
     *
     * @throws SolverException if no model or only model has been added.
     */
    public void run() {
        nThreads = Math.min(suppliers.size(),
                Math.min(nThreads, Runtime.getRuntime().availableProcessors()));
        System.out.printf("%d models to solve with up to %d active threads\n", suppliers.size(), nThreads);
        ExecutorService executor = Executors.newFixedThreadPool(nThreads);
        ExecutorCompletionService<Long> ecs = new ExecutorCompletionService<>(executor);
        List<Future<Long>> futures = new ArrayList<>();
        for (Supplier<Model> sup : suppliers) {
            futures.add(ecs.submit(() -> supply(sup)));
        }
        for (Future<Long> fut : futures) {
            try {
                fut.get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }
        executor.shutdownNow();
    }

    private Long supply(Supplier<Model> supplier) {
        Model model = supplier.get();
        synchronized (models) {
            models.add(model);
        }
        Solver solver = model.getSolver();
        solver.plugMonitor((IMonitorSolution) () -> updateFromSolution(model));
        switch (model.getResolutionPolicy()) {
            case SATISFACTION:
                break;
            case MINIMIZE:
                solver.getObjectiveManager().updateBestUB(bestBound.get());
                break;
            case MAXIMIZE:
                solver.getObjectiveManager().updateBestLB(bestBound.get());
                break;
        }
        solver.printShortFeatures();
        while (model.getSolver().solve()) {
            // void
        }
        solver.printShortStatistics();
        System.out.printf("%s leaves the race !\n", model.getName());
        synchronized (models) {
            models.remove(model);
        }
        return model.getSolver().getSolutionCount();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////   INTERNAL METHODS    //////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    private synchronized void updateFromSolution(Model m) {
        if (m.getResolutionPolicy().equals(ResolutionPolicy.SATISFACTION)) {
            // In a satisfaction problem, we assume that each worker has a distinct search space
            // then nothing is done when a solution is found, since we look for all of them
            synchronized (buffer) {
                ((IMonitorSolution) m.getHook("PRINTER")).onSolution();
                try {
                    Files.write(Paths.get(outputFile), buffer.toString().getBytes());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return;
        }
        int solverVal = ((IntVar) m.getObjective()).getValue();
        int bestVal = m.getSolver().getObjectiveManager().getBestSolutionValue().intValue();
        if (solverVal == bestVal) {
            bestBound.set(bestVal);
            System.out.printf("%s shares %s ...\n", m.getName(), m.getSolver().getObjectiveManager().getBestSolutionValue());
            synchronized (buffer) {
                buffer.setLength(0); // we only store the best solution
                ((IMonitorSolution) m.getHook("PRINTER")).onSolution();
                try {
                    Files.deleteIfExists(Paths.get(outputFile));
                    Files.write(Paths.get(outputFile), buffer.toString().getBytes());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            synchronized (models) {
                if (m.getResolutionPolicy().equals(ResolutionPolicy.MAXIMIZE)) {
                    models.forEach((mo1) -> mo1.getSolver().getObjectiveManager().updateBestLB(bestVal));
                } else {
                    models.forEach((mo1) -> mo1.getSolver().getObjectiveManager().updateBestUB(bestVal));
                }
            }
        }

    }
}

