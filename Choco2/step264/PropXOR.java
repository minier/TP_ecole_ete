/**
 * Copyright (c) 1999-2020, Ecole des Mines de Nantes
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * <p>
 * * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * * Neither the name of the Ecole des Mines de Nantes nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 * <p>
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

import org.chocosolver.solver.constraints.Propagator;
import org.chocosolver.solver.constraints.PropagatorPriority;
import org.chocosolver.solver.exception.ContradictionException;
import org.chocosolver.solver.exception.SolverException;
import org.chocosolver.solver.variables.IntVar;
import org.chocosolver.solver.variables.events.IntEventType;
import org.chocosolver.util.ESat;
import org.chocosolver.util.objects.setDataStructures.iterable.IntIterableRangeSet;

/**
 * <br/>
 *
 * @author Charles Prud'homme
 * @since 18/05/2020
 */
public class PropXOR extends Propagator<IntVar> {

    static int HIGHEST_BIT = 8;
    static int MAX_VALUE = (int) Math.pow(2, HIGHEST_BIT) - 1;

    IntIterableRangeSet set;

    public PropXOR(IntVar a, IntVar b, IntVar c) {
        super(new IntVar[]{a, b, c}, PropagatorPriority.TERNARY, false);
        set = new IntIterableRangeSet();
    }

    @Override
    public int getPropagationConditions(int vIdx) {
        return IntEventType.ALL_EVENTS;
    }

    public void filter(int i, int j, int k) throws ContradictionException {
        if (vars[i].getDomainSize() + vars[j].getDomainSize() <= MAX_VALUE) {
            set.clear();
            for (int v1 = vars[i].getLB(); v1 <= vars[i].getUB(); v1 = vars[i].nextValue(v1)) {
                for (int v2 = vars[j].getLB(); v2 <= vars[j].getUB(); v2 = vars[j].nextValue(v2)) {
                    set.add(v1 ^ v2);
                    if (set.size() == MAX_VALUE) {
                        return;
                    }
                }
            }
            vars[k].removeAllValuesBut(set, this);
        }
    }

    @Override
    public void propagate(int evtmask) throws ContradictionException {
        IntVar X = vars[0];
        IntVar Y = vars[1];
        IntVar Z = vars[2];
        int mask;
        mask = 0;
        mask += X.isInstantiated() ? 1 : 0;
        mask += Y.isInstantiated() ? 2 : 0;
        mask += Z.isInstantiated() ? 4 : 0;
        int vx, vy, vz;
        switch (mask) {
            case 0: // nothing instanciated
                filter(0, 1, 2);
                filter(0, 2, 1);
                filter(1, 2, 0);
                break;
            case 1: // X is instanciated
                filter(0, 1, 2);
                filter(0, 2, 1);
                break;
            case 2: // Y is instanciated
                filter(1, 0, 2);
                filter(1, 2, 0);
                break;
            case 4: // Z is instanciated
                filter(2, 0, 1);
                filter(2, 1, 0);
                break;
            case 3: // X and Y are instanciated
                filter(0, 1, 2);
                break;
            case 5: // X and Z are instanciated
                filter(0, 2, 1);
                break;
            case 6: // Y and Z are instanciatedvx = X.getValue();
                filter(1, 2, 0);
                break;
            case 7: // X, Y and Z are instanciated
                vx = X.getValue();
                vy = Y.getValue();
                vz = Z.getValue();
                int val = vx ^ vy;
                if (vz != val /*|| val > 255 && vx > 255 || vy > 255*/) {
                    fails();
                }
                break;
            default:
                throw new SolverException("Unexpected mask " + mask);
        }
    }


    @Override
    public ESat isEntailed() {
        if (isCompletelyInstantiated()) {
            int i = vars[0].getValue();
            int j = vars[1].getValue();
            int k = vars[2].getValue();
            return ESat.eval((i ^ j) == k);
        }
        return ESat.UNDEFINED;
    }
}
