/**
 * Copyright (c) 1999-2020, Ecole des Mines de Nantes
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * <p>
 * * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * * Neither the name of the Ecole des Mines de Nantes nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 * <p>
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

import org.chocosolver.solver.Model;
import org.chocosolver.solver.constraints.extension.Tuples;
import org.chocosolver.solver.variables.IntVar;

/**
 * <br/>
 *
 * @author Stéphanie Delaune
 * @since 10/04/2020
 */
public class Step2Factory {

    protected static final String EOL = System.getProperty("line.separator");

    protected static final int MAXVAL = 15;

    private static final int[] Sbox4 = new int[]{
        0xc,0x6,0x9,0x0,0x1,0xa,0x2,0xb,0x3,0x8,0x5,0xd,0x4,0xe,0x7,0xf
    };

    protected static final Tuples tupleXor = createRelationXor();

    protected static final Tuples tupleSB = createRelationSbox();

    private static Tuples createRelationXor() {
        Tuples tuplesXor = new Tuples(true);
        for (int i = 0; i < MAXVAL + 1; i++) {
            for (int j = 0; j < MAXVAL + 1; j++) {
                tuplesXor.add(i, j, i ^ j);
            }
        }
        return tuplesXor;
    }

    protected static Tuples createRelationSbox() {
        int[][] ddt = new int[MAXVAL + 1][MAXVAL + 1];
        for (int i = 0; i < MAXVAL + 1; i++)
            for (int j = 0; j < MAXVAL + 1; j++)
                ddt[i][j] = 0;

        for (int i = 0; i < MAXVAL + 1; i++) {
            for (int j = 0; j < MAXVAL + 1; j++) {
                {
                    ddt[i][Sbox4[j] ^ Sbox4[i ^ j]]++;
                }
            }
        }


        Tuples tuplesSB = new Tuples(true);
        tuplesSB.add(0, 0, 0);


        for (int i = 1; i < MAXVAL + 1; i++) {
            for (int j = 1; j < MAXVAL + 1; j++) {
                if (ddt[i][j] != 0) {
                    if (ddt[i][j] == 2) {
                        tuplesSB.add(i, j, 3);
                    } //2^-3 --> 3
                    if (ddt[i][j] == 4) {
                        tuplesSB.add(i, j, 2);
                    }  // 2^-2 --> 2
                }
            }
        }


        return tuplesSB;
    }


    protected static void postXOR(Model model, IntVar a, IntVar b, IntVar c) {
        model.table(new IntVar[]{a, b, c}, tupleXor).post();
    }

    // Add the xor constraint by considering the zeros. If there is a zero, the two other variables are equal
    protected static void postXOR(Model m, IntVar a, boolean a_zero, IntVar b, boolean b_zero, IntVar c, boolean c_zero) {
        if (a_zero) {
            b.eq(c).post();
        } else if (b_zero) {
            a.eq(c).post();
        } else if (c_zero) {
            a.eq(b).post();
        } else {
            postXOR(m, a, b, c);
        }
    }


    protected static void postMC(Model m, IntVar a, boolean a_zero, IntVar b, boolean b_zero, IntVar c, boolean c_zero, IntVar d, boolean d_zero, IntVar e, boolean e_zero, IntVar f, boolean f_zero, IntVar g, boolean g_zero, IntVar h, boolean h_zero) {
        a.eq(f).post();
        postXOR(m, b, b_zero, c, c_zero, g, g_zero);
        postXOR(m, a, a_zero, c, c_zero, h, h_zero);
        postXOR(m, d, d_zero, e, e_zero, h, h_zero);
    }


}
